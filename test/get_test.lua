local lu = require('luaunit')
local t = lu.named_scope('get')

local init = require('init')

local key = 13
local function subject(k)
  return init.get(k or key)
end

t.setUp = function()
  box.space.test:truncate()
end

t.missingKey = function()
  lu.assertEquals(subject(), nil)
end

t.existingKey = function()
  local value = {key, 2}
  box.space.test:put(value)
  lu.assertEquals(subject():totable(), value)
  lu.assertEquals(subject(key + 1), nil)
end

t.failed = function()
  lu.assertEquals(1, 2)
  shouldNotBeCalled()
end
