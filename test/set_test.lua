local lu = require('luaunit')
local t = lu.named_scope('set')

local init = require('init')

local key = 13
local value = {key, 2}

local function subject(value)
  init.set(value)
end

t.setUp = function()
  box.space.test:truncate()
end

t.missingKey = function()
  lu.assertEquals(box.space.test:get(key), nil)
  subject(value)
  lu.assertEquals(box.space.test:get(key):totable(), value)
end

t.existingKey = function()
  box.space.test:put({key, 3})
  subject(value)
  lu.assertEquals(box.space.test:get(key), value)
end

t.failed = function()
  lu.assertEquals(1, 2)
  shouldNotBeCalled()
end
