```
./run_luaunit [file_or_directory, default to ./test/] [--luaunit-flags ...] [list of test names]
```

```
./run_luaunit
./run_luaunit test/set_test.lua
./run_luaunit set.existingKey
./run_luaunit set.existingKey get.failed
./run_luaunit -p get.
```
