box.cfg()

local s = box.schema.space.create('test', {
  if_not_exists = true,
  format = {
    {'a', 'unsigned'},
    {'b', 'unsigned'},
  }
})

s:create_index('test_a', {
  if_not_exists = true,
  type = 'TREE',
  unique = true,
  parts = {'a'},
})

local export = {}

function export.get(id)
  return box.space.test:get(id)
end

function export.set(value)
  box.space.test:put(value)
end

return export
